﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TechnologieSieciowe.Entities;
using TechnologieSieciowe.Services;

namespace TechnologieSieciowe.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
        // GET: api/Accounts
        [HttpGet]
        public IEnumerable<Account> Get()
        {
            return Database.Accounts;
        }

        // GET: api/Accounts/5
        [HttpGet("{id}")]
        public ActionResult<Account> Get(Guid id)
        {
            Account account = Database.Accounts.FirstOrDefault(x => x.Id == id);
            if (account == null)
                return NotFound();
            return account;
        }

        // GET: api/Accounts/5
        [HttpGet("{id}/balance")]
        public ActionResult<double> GetBalance(Guid id)
        {
            Account account = Database.Accounts.FirstOrDefault(x => x.Id == id);
            if (account == null)
                return NotFound();
            return account.Balance;
        }

        // POST: api/Accounts
        [HttpPost]
        public ActionResult<Account> Post([FromBody] Account account)
        {
            Database.Accounts.Add(account);
            return account;
        }

        // POST: api/Accounts/51/transfer/515
        [HttpPost("{from}/transfer/{to}")]
        public ActionResult<Account> Transfer(Guid from, Guid to, [FromBody] double value)
        {
            Account accountFrom = Database.Accounts.FirstOrDefault(x => x.Id == from);
            Account accountTo = Database.Accounts.FirstOrDefault(x => x.Id == to);
            if (accountFrom == null || accountTo == null)
                return NotFound();
            if (value < accountFrom.Balance)
                return Conflict();

            accountFrom.Balance -= value;
            accountTo.Balance += value;
            return Ok();
        }

        // PUT: api/Accounts/5/balance
        [HttpPut("{id}/balance")]
        public ActionResult<Account> Put(Guid id, [FromBody] double value)
        {
            Account account = Database.Accounts.FirstOrDefault(x => x.Id == id);
            if (account == null)
                return NotFound();
            account.Balance = value;
            return account;
        }

        // DELETE: api/Accounts/5
        [HttpDelete("{id}")]
        public ActionResult Delete(Guid id)
        {
            Account account = Database.Accounts.FirstOrDefault(x => x.Id == id);
            if (account == null)
                return NotFound();
            Database.Accounts.Remove(account);
            return Ok();
        }
    }
}
