﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TechnologieSieciowe.Entities;

namespace TechnologieSieciowe.Services
{
    public static class Database
    {
        public static List<Account> Accounts { get; set; } = new List<Account>();
    }
}
