﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TechnologieSieciowe.Entities
{
    public class Account
    {
        public Guid Id { get; set; } = Guid.NewGuid();  // Generuje nowy unikalny identyfikator
        public DateTime Created { get; set; } = DateTime.Now;
        public string Owner { get; set; } = "";
        public double Balance { get; set; } = 0;
    }
}
